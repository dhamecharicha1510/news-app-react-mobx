import React from "react";
import { render } from "react-dom";
import App from "./App";
import NewsStore from './store/NewsStore';
const newsStore = new NewsStore();

render(
  <App store={newsStore} />,
  document.getElementById("root")
);
