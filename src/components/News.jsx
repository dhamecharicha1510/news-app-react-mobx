import React, { Fragment } from "react";
import InfiniteScroll from "react-infinite-scroll-component";
import { observer } from "mobx-react";
import {Form, Table} from 'react-bootstrap';

const style = {
  height: 'auto',
  border: "1px solid green",
  margin: 6,
  padding: 8
};

@observer
class News extends React.Component {
    constructor(props){
        super(props);  
        this.state = {
            hasMore: true
        };
    }

    fetchMoreData = () => {
        if (this.props.store.items.length >= this.props.store.filteredData.length) {
          this.setState({hasMore: false});
          return;
        }
       
        setTimeout(() => {
          this.props.store.fetchMoreData();
        }, 1500);
    };

    onSearchNews = (e) => {       
        this.props.store.onApplySearch(e.target.value); 
        if (!this.state.hasMore && ! (this.props.store.items.length >= this.props.store.filteredData.length)) {
            this.setState({hasMore: true});
        }
    };

    onApplyFilter = (e) => {        
        this.props.store.onApplyFilter(e.target.value); 
        if (!this.state.hasMore && ! (this.props.store.items.length >= this.props.store.filteredData.length)) {
            this.setState({hasMore: true});
        }
    };

    onChangeSelectedFormat = (e) => {
        this.props.store.changeSelectedFormat(e.target.value); 
        if (!this.state.hasMore && ! (this.props.store.items.length >= this.props.store.filteredData.length)) {
            this.setState({hasMore: true});
        }
    }

  render() {
    let {store} = this.props;
    if(!store.newsData.length) return <div>Loading</div>
    if(!store.selectedFormat) return <div style={{width:'800px', margin: 'auto'}}>Select your desired format from the dropdown first.</div>
    return (
        <div style={{width:'1200px', margin: 'auto'}}>
        
        <Form style={{width: '60%', margin:'20px auto'}}>
        <Form.Group controlId="Format" className="row">
            <Form.Label className="col-md-4">Show News By</Form.Label>
            <Form.Control as="select" custom className="col-md-8" onChange={this.onChangeSelectedFormat}>
            <option value="list">List</option>
            <option value="grid">Grid</option>
            </Form.Control>
        </Form.Group>
        </Form>  
        <h1>Breaking news only for you!</h1>
        <Form style={{width: '80%'}}>
        <Form.Group controlId="Filter" className="row">
            <Form.Label className="col-md-3">Filter By</Form.Label>
            <Form.Control as="select" custom className="col-md-4" onChange={(e) => {store.selectedFilter = e.target.value}}>
            <option value="source">Source</option>
            <option value="title">Title</option>
            <option value="content">Content</option>
            <option value="description">Description</option>
            <option value="author">Author</option>
            </Form.Control>
            <div className="col-md-1"/>
            <Form.Control
                type="text"
                name="filterValue"
                placeholder="Enter value to filter"
                className="col-md-4"
                onChange={this.onApplyFilter}
              />
        </Form.Group>
        </Form>
        
        <Form style={{width: '80%'}}>
        <Form.Group controlId="SearchNews" className="row">
            <Form.Label className="col-md-3">Search News:</Form.Label>
            <Form.Control
                type="text"
                name="searchNews"
                placeholder="Enter value to search news"
                className="col-md-9"
                onChange={this.onSearchNews}
              />
        </Form.Group>
        </Form>
        {!store.filteredData.length ? <div>No result found</div> : 
        <Fragment>
        <hr />
        <InfiniteScroll
            dataLength={store.items.length}
            next={this.fetchMoreData}
            hasMore={this.state.hasMore}
            loader={<h4 style={{ textAlign: "center", color: 'green' }}>Loading...</h4>}
            endMessage={
            <p style={{ textAlign: "center", color: 'green' }}>
                <b>You have reached at end of your results</b>
            </p>
            }
        >
        {
            store.selectedFormat === 'grid' ? <Table>
            <thead>
                <tr>
                    <th>Title</th>
                    <th>Description</th>                    
                    <th>Content</th>
                    <th>Author</th>
                    <th>Source</th>
                    <th>Image</th>
                    <th>Info</th>
                </tr>
            </thead>
            <tbody>
        {store.items.map((i, index) => (
            <tr key={index}>
                <td>{i.title || ''}</td>                
                <td>{i.description || ''}</td>                
                <td>{i.content || ''}</td>             
                <td>{i.author || ''}</td>
                <td>{i.source.name || ''}</td>
                <td><img src={i.urlToImage} height="80px" width="120px" alt={i.title}/></td>
                <td><a href={i.url}>Read More</a></td>
            </tr>

            ))}
            </tbody>
        </Table>
        :
        store.items.map((i, index) => (
            <div style={style} key={index}>
            <h2>{i.title}</h2>
                <p style={{textAlign: 'right'}}>Source: {i.source.name || ''}</p>
                <p style={{textAlign: 'right'}}>Published At {new Date(i.publishedAt).toDateString() || ''}</p>
                <div style={{textAlign: 'center'}}>
                    <img src={i.urlToImage} height="50%" width="90%" align='center' alt={i.title}/>
                </div>
                <p style={{fontWeight: 'bold'}}>{i.description} <br/><br/> {i.content}<a href={i.url}>Read More</a></p>
                <p style={{textAlign: 'right'}}>-{i.author || 'Anonymous'}</p>
            </div>))
        }        
        </InfiniteScroll>
        </Fragment>
        }
        </div>
    );
  }
}

export default News;
