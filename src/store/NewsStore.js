import { observable } from "mobx";
import axios from 'axios';

export default class NewsStore {
  @observable newsData = [];
  @observable currentEndLimit = 5;
  @observable items = [];
  @observable selectedFilter = 'source'
  @observable filteredData = [];
  @observable selectedFormat = 'list';

  constructor() {
    axios.get("http://newsapi.org/v2/top-headlines?country=us&category=business&apiKey=00b266812a8b4a3bbd384aac4bd24db8").then((res) => {
      this.newsData = res.data.articles;
      this.filteredData = res.data.articles
      this.items = Object.assign([], this.filteredData.slice(0, 5));      
      this.currentEndLimit = 5;
    })   
  }

  fetchMoreData = () => {
    this.currentEndLimit = ((this.currentEndLimit + 5) > this.filteredData.length) ? this.filteredData.length : (this.currentEndLimit + 5);
    this.items =  Object.assign([], this.filteredData.slice(0, this.currentEndLimit));
  }

  onApplyFilter = (filterValue) => {
    let data = Object.assign([], this.newsData);
    if(filterValue){
      if(this.selectedFilter === 'source'){
        this.filteredData = Object.assign([], data.filter((a) => (a.source.name.toLowerCase()).includes(filterValue.toLowerCase())));
      }
      else{
        console.log(this.selectedFilter, data);
        this.filteredData = Object.assign([], data.filter((a) => a[this.selectedFilter] && (a[this.selectedFilter].toLowerCase()).includes(filterValue.toLowerCase())));
      }
      
    }
    else{
      this.filteredData = Object.assign([], this.newsData);
    }
    this.items = Object.assign([], this.filteredData.slice(0, 5));
    this.currentEndLimit = 5;
  }


  onApplySearch = (searchValue) => {
    let data = Object.assign([], this.newsData);
    if(searchValue){
      this.filteredData = Object.assign([],data.filter((a) => 
        (a.description && a.description.toLowerCase().includes(searchValue.toLowerCase()) || 
        a.title && a.title.toLowerCase().includes(searchValue.toLowerCase()) || 
        a.author && a.author.toLowerCase().includes(searchValue.toLowerCase()) || 
        a.source.name && a.source.name.toLowerCase().includes(searchValue.toLowerCase()) ||
        a.content && a.content.toLowerCase().includes(searchValue.toLowerCase())
      )));    
    }
    else{
      this.filteredData = Object.assign([], this.newsData);
    }
    this.items = Object.assign([], this.filteredData.slice(0, 5));
    this.currentEndLimit = 5;
  }


  changeSelectedFormat = (value) => {
    this.selectedFormat = value;
    this.selectedFilter = 'source';
    this.filteredData = Object.assign([], this.newsData);
    this.items = Object.assign([], this.filteredData.slice(0, 5));
    this.currentEndLimit = 5;
  }
}
