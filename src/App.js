import React, { Component } from "react";
import { observer } from "mobx-react";
import News from "./components/News";

@observer
class App extends React.Component {
  render() {
    return (  
        <div>     
            <News store={this.props.store} />
        </div>      
    );
  }
}

export default App;
